package home

import (
	"gitee.com/jikey/elk-blog/app/service"
	"gitee.com/jikey/elk-blog/pkg/response"
	"gitee.com/jikey/elk-blog/pkg/utils"
	"github.com/gin-gonic/gin"
	"html/template"
	"strconv"
	"strings"
)

type Writer struct {
	Controller
}

// Index 首页
func (w *Writer) Index(c *gin.Context) {
	var conditions []string
	data := make(map[string]interface{})
	pageNum, _ := strconv.Atoi(c.Query("page"))
	maps := ""
	pageSize := 12

	conditions = append(conditions, "tk_user.status = 1")
	maps = strings.Join(conditions, " AND ")

	// 获取分页数据
	rows, total := service.User.List(pageNum, pageSize, maps)
	page := utils.NewPagination(c.Request, int(total), pageSize)
	data["page"] = template.HTML(page.Pages())
	data["list"] = rows
	data["link"] = service.Link.List(1, 100, "")
	data["archiveList"] = service.Article.ArticleArchiveList()
	data["articleCategory"] = service.Category.GetArticleCategoryList("4")
	data["site"] = service.System.GetSystem(1)
	data["active"] = "writer"

	response.Render(c, "writer/index", data)
}
