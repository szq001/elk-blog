package admin

import (
	"github.com/gin-gonic/gin"
)

type Base struct {
	Controller
}

var base *Base

func init() {
	base = &Base{}
	_ = base.Init(base)
}

func (base *Base) List(c *gin.Context) {
}
