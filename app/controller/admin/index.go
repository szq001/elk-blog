package admin

import (
	"gitee.com/jikey/elk-blog/pkg/response"
	"gitee.com/jikey/elk-blog/pkg/utils"
	"github.com/flosch/pongo2"
	"github.com/gin-gonic/gin"
	"github.com/vjeantet/jodaTime"
	"runtime"
	"time"
)

type Index struct{}

func (_ *Index) Index(c *gin.Context) {
	data := make(map[string]interface{})

	data["site"] = Site
	info := map[string]interface{}{
		"语言":       runtime.Version(),
		"版本":       "v-0.1",
		"当前时间":     utils.GetTime(),
		"北京时间":     jodaTime.Format("yyyy年MM月dd HH:mm:ss", time.Now()),
		"服务器域名/IP": c.ClientIP(),
		"操作系统":     runtime.GOOS,
		"Host":     c.ClientIP(),
	}

	c.HTML(200, "admin/index/index.html", pongo2.Context{
		"info": info,
		"data": data,
	})

}

// Welcome 欢迎
func (_ *Index) Welcome(c *gin.Context) {
	info := map[string]interface{}{
		"语言":       runtime.Version(),
		"版本":       "v-0.1",
		"当前时间":     utils.GetTime(),
		"北京时间":     jodaTime.Format("yyyy年MM月dd HH:mm:ss", time.Now()),
		"服务器域名/IP": c.ClientIP(),
		"操作系统":     runtime.GOOS,
		"Host":     c.ClientIP(),
	}

	c.HTML(200, "admin/index/welcome.html", pongo2.Context{
		"info": info,
	})
}

type Student struct {
	Name  string `json:"name"`
	Age   int
	Class *Class `json:"class"`
}

type Class struct {
	Name  string
	Grade int `json:"grade"`
}

type Rank struct {
	// 用到json记得一定大写首字母
	UserId string `json:"userId"`
	Name   string `json:"name"`
}

// Test 欢迎
func (_ *Index) Test(c *gin.Context) {
	// fmt.Println("Test test")
	student := Student{
		Name: "李四",
		Age:  14,
	}
	//
	class := new(Class)
	class.Name = "608班"
	class.Grade = 6
	student.Class = class
	//
	if err := c.ShouldBind(&student); err != nil {
		response.JSON(c, student)
		return
	}

	response.JSON(c, student)
}
