package admin

import (
	"fmt"
	"gitee.com/jikey/elk-blog/app/model"
	"gitee.com/jikey/elk-blog/app/service"
	"gitee.com/jikey/elk-blog/pkg/e"
	"gitee.com/jikey/elk-blog/pkg/response"
	"gitee.com/jikey/elk-blog/setting"
	"github.com/flosch/pongo2"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"strconv"
	"strings"
)

type Article struct {
	Base
}

func (b *Article) Index(c *gin.Context) {
	c.HTML(200, "admin/article/index.html", pongo2.Context{
		"title": "Welcome11222!",
		"greet": "hello",
		"obj":   "world",
	})
}

// List 列表
func (b *Article) List(c *gin.Context) {
	data := make(map[string]interface{})
	var filters []string
	maps := ""

	pageNum, _ := strconv.Atoi(c.DefaultQuery("page", "1"))

	title := c.Query("title")
	userName := c.GetString("current_user_name")

	if len(title) > 0 {
		filters = append(filters, fmt.Sprintf("title like '%%%s%%'", title))
	}

	// 根据用户筛选文章
	if len(userName) > 0 && userName != "admin" {
		filters = append(filters, fmt.Sprintf("username like '%%%s%%'", userName))
	}

	maps = strings.Join(filters, " AND ")

	data["rows"] = service.Article.ArticleList(pageNum, setting.Config.App.PageSize, maps)
	data["total"] = service.Article.GetArticleTotal(maps)

	response.Success(c, data, "列表返回成功了呀！")
}

// Detail 详情
func (b *Article) Detail(c *gin.Context) {
	data := make(map[string]interface{})

	id, _ := strconv.Atoi(c.Param("id"))
	data["rows"], _ = service.Article.ArticleDetail(id)

	response.Success(c, data)
}

// Category 种类渲染页面
func (b *Article) Category(c *gin.Context) {
	response.HTML(c, "category/index", nil)
}

// CategoryList 获取列表数据
func (b *Article) CategoryList(c *gin.Context) {
	data := make(map[string]interface{})
	data["rows"] = service.Category.GetArticleCategoryList("4")

	response.Success(c, data)
}

// Edit 文章新增、编辑
func (b *Article) Edit(c *gin.Context) {
	response.HTML(c, "article/edit", nil)
}

// EditMd markdown文章新增、编辑
func (b *Article) EditMd(c *gin.Context) {
	response.HTML(c, "article/md", nil)
}

// Insert 新增保存
func (b *Article) Insert(c *gin.Context) {
	Article := &model.Article{}
	Article.Username = c.GetString("current_user_name")

	if err := c.ShouldBind(&Article); err != nil {
		c.JSON(400, gin.H{
			"err": err.Error(),
		})
		return
	}

	// Article.Content = utils.RepImages(Article.Content)
	err := service.Article.Create(Article)

	if err != nil {
		logrus.Error("新增失败", err)
		response.Fail(c, e.ErrorUpdate)
		return
	}

	response.Success(c, Article)
}

// Update 修改
func (b *Article) Update(c *gin.Context) {
	Article := &model.Article{}

	if err := c.ShouldBind(&Article); err != nil {
		c.JSON(400, gin.H{
			"err": err.Error(),
		})
		return
	}

	id := strconv.Itoa(Article.Id)
	err := service.Article.Update(id, Article)

	if err != nil {
		logrus.Error("修改失败", err)
		response.Fail(c, e.ErrorUpdate)
		return
	}

	response.Success(c, Article)
}

// Destroy 删除
func (b *Article) Destroy(c *gin.Context) {
	id := c.PostForm("id")
	service.Article.Delete(id)

	response.Success(c, id)
}
