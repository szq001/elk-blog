package admin

import (
	"gitee.com/jikey/elk-blog/pkg/response"
	"gitee.com/jikey/elk-blog/pkg/utils"
	"github.com/gin-gonic/gin"
	"github.com/shirou/gopsutil/host"
	"github.com/shirou/gopsutil/mem"
)

type Server struct {
	Base
}

// List 列表
func (s *Server) List(c *gin.Context) {
	data := make(map[string]interface{})

	data["cpu"] = utils.GetCpuPercent()
	data["mem"], _ = mem.VirtualMemory()
	data["sys"], _ = host.Info()

	response.HTML(c, "system/monitor", data)
}
