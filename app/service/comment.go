package service

import (
	"gitee.com/jikey/elk-blog/app/model"
	db "gitee.com/jikey/elk-blog/app/model"
)

type comment struct {
}

var Comment *comment

func (m *comment) List(PageNum int, PageSize int, maps interface{}) (comment []*model.Comment) {
	db.Mysql.Where(maps).Offset((PageNum - 1) * PageSize).Limit(PageSize).Order("id desc").Find(&comment)

	for _, reply := range comment {
		reply.Children = []*model.Comment{}
		db.Mysql.Where("pid = ?", reply.Id).Find(&reply.Children)
	}

	return
}

func (m *comment) Create(model *model.Comment) (err error) {
	err = db.Mysql.Create(model).Error

	return err
}

// Total 获取 blog 总记录数
func (m *comment) Total(maps interface{}) (count int) {
	db.Mysql.Model(&comment{}).Where(maps).Count(&count)
	return
}

// Delete 删除
func (m *comment) Delete(id string) bool {
	db.Mysql.Where("id = ?", id).Delete(&comment{})

	return true
}
