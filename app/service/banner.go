package service

import (
	"gitee.com/jikey/elk-blog/app/model"
	db "gitee.com/jikey/elk-blog/app/model"
)

type banner struct {
}

var Banner *banner

// List banner列表
func (b *banner) List(maps interface{}) (banner []*model.Banner) {
	db.Mysql.Where(maps).Order("ord asc, id desc").Find(&banner)
	return
}

// Create 新增
func (b *banner) Create(model *model.Banner) (err error) {
	err = db.Mysql.Create(model).Error

	return err
}

// Update 修改
func (b *banner) Update(id string, model *model.Banner) (err error) {
	err = db.Mysql.Model(&model).Where("id = ? ", id).Updates(model).Error

	return err
}

// Delete 删除
func (b *banner) Delete(id string) bool {
	db.Mysql.Where("id = ?", id).Delete(&banner{})

	return true
}
