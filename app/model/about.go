package model

type About struct {
	BaseModel
	Content string `gorm:"column:content" json:"content" form:"content"`
}
