package model

type Banner struct {
	BaseModel
	ModelId  int    `gorm:"column:modelId" json:"modelId"`
	Smallimg string `gorm:"column:smallimg" json:"smallimg"`
	Link     string `gorm:"column:link" json:"link"`
	Title    string `gorm:"column:title" json:"title"`
}
