package model

import (
	"errors"
	"gitee.com/jikey/elk-blog/pkg/utils"
	"github.com/jinzhu/gorm"
)

type ToUserType struct {
	Username string `gorm:"column:username" json:"username" form:"username" binding:"required"`
	Content  string `gorm:"column:content" json:"content" form:"content"`
}

type Message struct {
	BaseModel
	Username  string        `gorm:"column:username" json:"username" form:"username" binding:"required"`
	Email     string        `gorm:"column:email" json:"email" form:"email"`
	Url       string        `gorm:"column:url" json:"url" form:"url"`
	Content   string        `gorm:"column:content" json:"content" form:"content"`
	ToUser    []*ToUserType `json:"ToUserType"`
	Verify    string
	CaptchaId string `gorm:"-"`
}

func (d *Message) IsValid() (err error) {
	if d.Email == "" {
		err = errors.New("标题不能为空")
	}
	return
}

// AfterFind 返回前加密数据
func (d *Message) AfterFind(tx *gorm.DB) (err error) {
	if d.Email != "" {
		d.Email = utils.Md5(d.Email)
	}

	return
}
