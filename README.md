大家好，我在公司的岗位是前端，由于对Go的广泛兴趣到最后的挚爱，奔着学习的目的开发了这款基于`Go, Gin, Gorm, Mysql, Vue, Element, Pongo2` 的个人博客，近期1.0版本正式发布了。
第一次用go开发东西，肯定有很多不足，请大佬们轻拍。
一句话总结一下：

1. 前后端不分离，没有npm安装打包的操作，模板由[Pongo2](https://github.com/flosch/pongo2)模板引擎负责呈现。
2. 后台管理页面在 Layuimini 的基础上采用 iframe+vue 混合开发方式，原来通过 iframe 实现多标签的功能及基础布局框架没有变更，在业务页面直接引入 vue，element。好处是省去安装打包的麻烦，缺点是支撑不了复杂的业务。
3. 提供了三套不同模板风格，分别对应：完整呈现，图片为主，简洁风格。


仓库地址：
- [Gitee elk-blog](https://gitee.com/jikey/elk-blog)

预览地址：
- [http://milu.blog](http://milu.blog)

后台地址：
- [http://milu.blog/admin/login](http://milu.blog/admin/login)

共提供三款基础主题，在`系统管理 / 系统设置` 中设置。
- 绿色主题

![绿色主题.jpg](https://img-blog.csdnimg.cn/img_convert/15cc2823ba49c90a23c033e6cb73d739.jpeg)
- 默认主题

![默认主题.jpg](https://img-blog.csdnimg.cn/img_convert/fb70b2a2a6ca67765e747a3e38f2a37d.jpeg)
- 极简主题

![极简主题.jpg](https://s3.bmp.ovh/imgs/2023/02/10/ef69f4f05d8225df.jpg)
- 后台管理

![管理后台.jpg](https://img-blog.csdnimg.cn/img_convert/102fc0831b42ef27571c0f445941351f.jpeg)


### 1. 相关介绍

#### 1.1 基本介绍
- 取`麋鹿博客` 名字是为便于搜索引擎关键字直达，同时组成整个动物系列开源项目。总的思路就是：

    1. 简单的开发
    前后端不分离，后台管理虽然使用了`Vue 2.x`，但是不安装依赖不打包，直接引入使用。
    2. 简单的部署
    没有`docker`, `redis`，`FTP`直接上传，适合配置低的机型部署。
    3. 简单的维护
    `.go`文件打包发布后需要重启`go`应用，`public、view`等静态文件可单独热更新，不需要重启`go`服务。

- 开发时间从5到8差不多三个月的业余时间。
- 静态页面参考其它项目，没有投入太多的时间，只在细节上做了调整。


#### 1.2 功能特性

1. 文章

   - 文章发布，采用富文本 `tinymce` 和 `markdown` 编辑的使用体验
   - 分类管理
   - 文章排序
   - 草稿箱

2. Banner管理

   - Banner增加、修改、删除

3. 评论

   - 文章评论
   - 评论回复

4. 友情链接

   - 添加、修改、删除友链

5. 用户

   - 修改用户基础信息

6. 友情链接

   - 添加、修改友链
   - 友链分类
7. 设置

   - 网站设置
   - 主题切换


#### 1.2 技术栈

##### 1.2.1 后台
技术 | 说明 | 官网
----|----|----
Golang - 1.18 | 开发语言 | [https://go.dev/](https://go.dev/)
Gin - 1.8.1 | Gin Web Framework | [https://gin-gonic.com/zh-cn/docs/](https://gin-gonic.com/zh-cn/docs/)
Mysql - 5.7 | 数据库 | [https://www.mysql.com/cn/](https://www.mysql.com/cn/)
Gorm - 1.9.16 | Golang ORM | [https://gorm.io/zh_CN/docs/index.html](https://gorm.io/zh_CN/docs/index.html)
Jwt | Golang jwt | [https://github.com/golang-jwt/jwt](https://github.com/golang-jwt/jwt)
Pongo2 - 5 | 模板引擎 | [https://github.com/flosch/pongo2](https://github.com/flosch/pongo2)
Logrus | 日志 | [https://github.com/sirupsen/logrus](https://github.com/sirupsen/logrus)
Base64Captcha | 验证码 | [https://github.com/mojocn/base64Captcha](https://github.com/mojocn/base64Captcha)
Crypto | 密码库 | [https://golang.org/x/crypto](https://golang.org/x/crypto)
Ini | ini文件库 | [https://github.com/go-ini/ini](https://github.com/go-ini/ini)
Goment | 时间处理工具 | [https://github.com/nleeper/goment](https://github.com/nleeper/goment)
Air | 热更新工具 | [https://github.com/cosmtrek/air](https://github.com/cosmtrek/air)

##### 1.2.1 前台
技术 | 说明 | 官网
----|----|----
Vue - 2.x | 渐进式JavaScript 框架 | [https://cn.vuejs.org/v2/guide/](https://cn.vuejs.org/v2/guide/)
Axios | 基于promise 的HTTP 库 | [https://github.com/axios/axios](https://github.com/axios/axios)
Element-UI | 前端UI组件库 | [https://element.eleme.io/](https://element.eleme.cn/#/zh-CN/)
Tinymce | 可视化HTML编辑器 | [https://www.tiny.cloud/](https://www.tiny.cloud/)
Fontawesome | 图标字体库  | [http://www.fontawesome.com.cn/](http://www.fontawesome.com.cn/)

#### 1.3 开发工具

系统 | 工具 | 官网
----|----|----
Goland | 开发工具 | [https://www.jetbrains.com/zh-cn/go/](https://www.jetbrains.com/zh-cn/go/)
Navicat | 数据库管理工具 | [https://www.navicat.com.cn/](https://www.navicat.com.cn/)
Atom | 源码阅读工具 | [https://atom.io/](https://atom.io/)
Cmder | Cmd替代工具[windows] | [https://cmder.net/](https://cmder.net/)
Notepad2 | 临时单文件编辑[windows] | [http://www.flos-freeware.ch/notepad2.html](http://www.flos-freeware.ch/notepad2.html)
Chrome | 调试工具 | [https://www.google.com/intl/zh-CN/chrome/](https://www.google.com/intl/zh-CN/chrome/)

#### 1.4 文件结构
整体的结构参考世上最优美的框架 [Laravel](https://laravel.com/)。
```javascript
├─app                 // 核心代码
│  ├─controller       // 控制层
│  │  ├─admin
│  │  └─home
│  ├─database        // 数据库链接
│  ├─model           // 模型层
│  └─service         // 操作数据层
├─config             // 配置文件
├─pkg                // 所有工具文件
│  ├─e               // 报错
│  ├─hash            // 验证码
│  ├─response        // 返回封装
│  └─utils           // 工具库
├─public             // 所有静态资源
│  ├─admin
│  ├─common
│  ├─data
│  ├─green
│  ├─home
│  └─uploads
├─routers             // 路由文件
└─views               // 所有静态资源
    ├─admin
    ├─green           // 绿色主题模板
    ├─default         // 默认模板
    └─home            // 普通模板
```

Tips：

- Air
由于go本身没有热加载技术，所以还需要一个热加载工具的支持。可选的也不多，
##### 1. [Fresh](https://github.com/gravityblast/fresh)
Fresh满足基础的应用，每次保存文件都会生成或重新启动Web应用程序，只是这工具多年未更新所以弃用。
##### 2. [Air](https://github.com/cosmtrek/air)
Air的优点也比较突出：彩色日志输出，自定义构建或二进制命令，支持忽略子目录，启动后支持监听新目录等等的。
###### 2.1 `Air` 存在问题
Air存在缓存问题，虽然在cmd里边结束Air，但刷新浏览器程序依然在运行，这时就需要手工结束进程然后重启。
```javascript
// 查找 PID，9888为端口号
netstat -ano | findstr 9888
// 杀死进程，14172 查到的pid
taskkill /pid 14172 /f
```
###### 2.2 Command not found 报错
如果输入 `air` 报这个错，那需要在系统的path里边配置项目路径，比如项目在`D:\go-project`，那么在path里边就应该有一条：
```
D:\go-project\bin
```
- [Pongo2](https://github.com/flosch/pongo2)
由于是前后端未分离的开发方式，所以模板引擎扮演着重要的角色，起初的选型也看了不少。比如[goview](https://github.com/foolin/goview)，仿ejs的[quicktemplate](https://github.com/valyala/quicktemplate)，还有类ejs的[hero](https://github.com/shiyanhui/hero)等等的。最后选定Pongo2的理由是功能强大，上手容易，能够容易实现分模板继承不同模板的需求。他大体的思路和语法是仿Jinja2和Django模板的或基本一样的。有诸如Extends、Macro、Block、Include等强大功能，如果有类 [Django](https://www.djangoproject.com/), [Nunjucks](https://mozilla.github.io/nunjucks/) 模板语法上手基本没有难度，Pongo2文档写的不是那么详细，有进一步了解还需要看仓库源码 [Template_tests](https://github.com/flosch/pongo2/tree/master/template_tests/)。
问题，与Vue的取值边界符号有冲突。

两种解决办法：
1. Vue的标签用v-html来代替，比如：`<div v-html="user.nickname"></div>`
2. 修改Vue的边界修饰符 [delimiters配置方式](https://cn.vuejs.org/v2/api/#delimiters)。
Goland对Pongo2并没有提供专门的语法高亮支持，这其中推荐另外一个插件，[Twig](https://www.jetbrains.com/help/idea/symfony-twig.html)，需要2步
1. 安装插件 `File -> Settings -> Plugins -> Twig`。
2. 重启开发工具

### 2. 如何在本地运行
以下以`windows`系统举例
先从[官网](https://go.dev/dl/)下载安装最新版`Go`开发包 `1.18.3`，然后进行相关的配置。

#### 2.1 环境变量的配置
在系统变量中添加 `Go` 开发相关的变量，需添加以下变量
变量名 | 值 | 说明
----|----|----
GOPATH | d:\go-project | `Go` 语言的开发目录
GOROOT | c:\Go | 安装 `Go` 安装目录
PATH | c:\Go\bin;d:\go-project\bin | 终端可以直接运行`Go`命令; 运行自己编译的`Go`程序和`Air`
GO111MODULE | on | 开启 `Go.mod` 功能，统一用`go.mod`管理开发依赖包，此功能在`Go1.11`版本中添加
GOPROXY | https://goproxy.cn | `Go` 包下载代理地址

#### 2.2 数据库

##### 2.2.1 `Mysql`的安装
`Mysql`是我以前玩`PHP`的套件[Phpstudy](https://www.xp.cn/)，有安装方便、启动便捷、管理省心等特点，当然也可以选择单独的 `Mysql` 来安装。由于最终发布的[宝塔](http://www.bt.cn)控制面板目前默认版本是 `5.7`，为了避免不必要的麻烦，我目前安装这是这一版本。
##### 2.2.2 数据导入
新建名称为 `elk-blog` 的数据库，注意字符集为 `utf8 -- UTF-8 Unicode`，字符编码为 `utf8_general_ci`
然后导入 `elk-blg/public/data/elk-blog.sql`文件。

- 然后克隆项目代码到本地：
```
$ git clone https://gitee.com/jikey/elk-blog.git
```

- 复制项目目录下文件 `config/env.example.ini` 为 `env.ini` 并修改配置文件中的数据库连接相关信息
```
[database]
type = mysql
host = localhost
port = 3306
user = root
password = root
dbname = elk-blog
charset = utf8
```
- 进入项目根目录，安装项目相关依赖
```
$ go mod download
```
- 进入项目根目录，启动项目，比如：
```
$ cd D:\go-project\src\elk-blog
$ air
```

前台入口：
```
http://localhost:8080/
```
后台入口：
```
http://localhost:8080/admin/login
```
初始用户名：`admin`，密码：`admin123$`


#### 2.3 `Goland` 的配置

##### 2.3.1 File -> Settings -> Go 下配置
变量名 | 值 | 说明
----|----|----
GOPATH | d:\go-project | `Go` 语言的开发目录
GOROOT | C:\Go | 会默认选择系统安装最高版本
GOPATH-Global GOPATH | d:\go-project | 全局设置
Go Modules | GOPROXY=https://goproxy.cn,direct | 先从配置地址下载，若失败，转从原始地址下载 功能，统一用`go.mod`管理开发依赖包，此功能在`Go1.11`版本中添加
GOPROXY | https://goproxy.cn | `Go` 包下载代理地址

##### 2.3.2 Settings -> Project Structure
排除`Exclued` `.idea, bin, pkg` 等目录，不进行索引，有效节省内存资源。
##### 2.3.3 Settings -> Appearance
使用`One Dark`主题


#### 2.4 `Air` 的配置

### 3. 如何上线发布
由于本人特别菜，对经典的`linux`及`docker`一直没怎么学会，所以这次选择的是[宝塔](http://www.bt.cn)控制面板来辅助部署，以下的经验也是基于宝塔来介绍。
#### 3.1 安装GO
[宝塔Linux面板-安装golang环境](https://www.bt.cn/bbs/thread-17882-1-1.html)

- 基本的步骤就是先从go官网下载`tar`包，然后上传到服务器指定目录上 `/usr/local`，这样做的目的是，能够快速完成节省时间。
- 然后解压添加环境变量 `tar -xzvf go1.18.2.linux-amd64.tar.gz`，这里边需要说明的是宝塔的终端不能修改配置文件，即没有退出保存的模式，需要ssh和直接在文本编辑模式修改。
我添加的环境变量：
```
export GOROOT=/usr/local/go # 设置为go安装的路径
export GOPATH=/www/wwwroot/GO #项目路径
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin
```
- 宝塔终端输入`go version`，显示：`go version go1.18.2 linux/amd64` 即确认为安装成功

#### 3.2 新建网站
其实就是新建nginx配置文件，比如新建网站 `milu.blog`，然后在nginx中配置go服务端口入口。假如go的端口为`4000`，则增加如下配置：
```
server{
  location / {
    proxy_pass  http://127.0.0.1:4000;
  }
}
```
#### 3.3 准备打包go
go的打包命令只打包`*.go`结尾的文件，换句话说非`*.go`的文件需要自己手动上传。如果经历过`npm run build`洗礼的人，那go的这个build至少到现在没有遇到大的问题，或许我代码写的少的原因。
```
set CGO_ENABLED=0
set GOARCH=amd64
set GOOS=linux
go build main.go
```
#### 3.4 上传文件
上传的目录为在环境变量里边定义的项目路径，比如milu项目的目录就是
```
/www/wwwroot/GO/milu
```
其它的项目就是
```
/www/wwwroot/GO/other
```
为了防止其它配置文件寻找不到，那所有相关的文件都在这个目录中。那最后发布完的`/www/wwwroot/GO/milu`下的文件组织为：
```
├─config  // 配置文件
├─public  // 静态资源
├─views   // 模板文件
├─main    // 打完包二进制文件
```
也就是说前面那么多文件，如果部署这些文件就够了。
#### 3.5 上传`SQL`文件
这块唯一说的是宝塔只支持`Mysql5.7`版本，暂不支持`Mysql8.0`，手工安装估计也可以，偷懒也没折腾。为防止版本不兼容，在本地开发时`Mysql`就为`5.7`。然后本地`Navicat`导出结构和数据，在宝塔的`Phpmyadmin`里边导入。
#### 3.6 命令行终端调试
在宝塔终端中cd到项目目录`/www/wwwroot/GO/milu`，然后直接 `./main`，这样方便的查看日志，能够看到一些详情的报错信息。运行 `./main`之后，就可以刷新域名，如果各步正常页面就能显示出来。
#### 3.7 PM2绑定进程
我也只是试试的心态用pm2来运行一下go，没想到还真运行起来了。这样暂时不安装其它的服务，和Node服务一块运行起来。
### 4. 感谢的人

此项目感谢以下各界人士的支持和帮助

- [上海-时间](https://github.com/guowenzhuang) 时佬在 `Mysql` 领域给我更多的帮助
- [南京-Wike](https://github.com/wike) `Wike`是一位资深的 `Go` 开发人士
- [上海-Sam](https://blog.lixhuan.com) `Sam` 擅长领域是 `Gin`, `Vue`

方便找回此项目，可以点右上角 💖Star 💖 收藏 + 支持

还可以
- Golang交流群 105545914
- Vue交流群 364912432
- Javascript交流群 492107297