/**
 * 主开发框架
 */
;(function (win) { // 主开发框架
    win.app = win.app || {}

    app.namespace = function (name, sep) {
        var s = name.split(sep || '.'),
            d = {},
            o = function (a, b, c) {
                if (c < b.length) {
                    if (!a[b[c]]) {
                        a[b[c]] = {}
                    }
                    d = a[b[c]]
                    o(a[b[c]], b, c + 1)
                }
            }

        o(window, s, 0)
        return d
    }

    app.namespace('app.cookie')

    $.extend(app.cookie, {
        set: function (name, value, days, path) {
            var exp = new Date()

            exp.setTime(exp.getTime() + days * 24 * 60 * 60 * 1000)

            var arr = document.cookie.match(new RegExp('(^| )' + name + '=([^;]*)(;|$)'))

            path = path ? `; path=${path}` : ''

            document.cookie = name + '=' + escape(value) + ';expires=' + exp.toUTCString() + path
        },
        get: function (name) {
            var arr = document.cookie.match(new RegExp('(^| )' + name + '=([^;]*)(;|$)'))
            if (arr != null) {
                return unescape(arr[2])
            }
            return null
        },
        remove: function (name) {
            var exp = new Date()
            exp.setTime(exp.getTime() - 1)
            var cval = app.cookie.get(name)
            if (cval != null) {
                document.cookie = name + '=' + cval + ';expires=' + exp.toUTCString()
            }
        }
    })

    app.loading = {
        _count: 0,
        _create: _.once(function () {
            var tpl = '<div id="app-loading" class="ui-loading"><div class="ui-loading-mask"></div><i></i></div>'
            // var tpl = '<div id="app-loading" class="ui-loading">';
            // tpl += '<span class="ui-loading-mask"></span>';
            // tpl += '<i></i>';
            // tpl += '<h3>正在加载....</h3></div>';
            $('body').append(tpl);
        }),
        show: function () {
            this._create()
            if (!this._count) {
                $('#app-loading').show()
            }
            this._count++
        },
        hide: function () {
            this._count--
            if (this._count <= 0) {
                this._count = 0
                $('#app-loading').hide()
            }
        }
    }

    // loading
    // app.loading = function (){
    //     app.loading.show()
    // }

    var AJAX_DEFAULTS = {
        type: 'get',
        dataType: 'json',
        timeout: 20000,
        beforeSend: function (data, status, xhr) {
            app.loading.show()
        },
        // complete: function (xhr, ex) {
        //     setTimeout(function() {
        //         app.loading.hide()
        //     }, 200)
        // },
        error: function (xhr, status, ex) {
            // require(['static/js/jym.dialog'], function (d) {
            //     d.message.show('网络不给力，请稍后再试！')
            // })
        }
    }

    $.ajaxSetup({
        complete: function (XMLHttpRequest, textStatus) {
            // debugger
            //通过XMLHttpRequest取得响应结果
            var res = XMLHttpRequest.responseText
            try {
                var jsonData = JSON.parse(res)
                if (jsonData.state == -1) {
                    //如果超时就处理 ，指定要跳转的页面(比如登陆页)
                    alert(jsonData.msg)
                    window.location.replace('/login/index.php')
                } else if (jsonData.state == 0) {
                    //其他的异常情况,给个提示。
                    alert(jsonData.msg)
                } else {
                    //正常情况就不统一处理了
                }
            } catch (e) {
            }
        }
    })

    // ajax 封装
    app.http = function (setting) {
        var ajaxOptions = $.extend({}, AJAX_DEFAULTS, setting)
        return $.ajax(ajaxOptions)
    }


}(window));


// core
(function (win, $) {
    var userAgent = window.navigator.userAgent

    $.extend(app, {
        version: '0.0.1',
        now: function () {
            return (new Date).getTime()
        },
        isOpera: function () {
            return !!window.opera
        },
        isFirefox: function () {
            return userAgent.indexOf('Firefox') > 0
        },
        isChrome: function () {
            return userAgent.indexOf('Chrome') > 0
        },
        isEmail: function () {
            var reg1 = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4})$/
            return reg1.test(this)
        },
        isMobile: function () {
            var reg = /^[1][3,4,5,8]\d{9}$/
            return reg.test(this)
        },
        /**
         * @description 手机类型判断
         * @param type
         * @return {*}
         */
        getBrowserInfo: function (type) {
            var typeObj = {
                android: 'android',
                iphone: 'android',
                ipad: 'ipad',
                weixin: 'micromessenger'
            }
            return type ? userAgent.toLowerCase().indexOf(typeObj[type]) !== -1 : userAgent.toLowerCase();
        },
        /**
         * 将url上的参数组装成以参数名称为属性名的对象
         */
        getParams: function () {
            var url = window.location.href
            var index = url.indexOf('?')
            var params = {}
            if (index !== -1) {
                var paramsStr = url.slice(index + 1) // 获取到问号以后的字符串
                var paramsArr = paramsStr.split('&')
                // 把url上的所有参数塞到json对象中,以键值对的方式保存
                for (var i = 0, length = paramsArr.length, param; i < length; i++) {
                    param = paramsArr[i].split('=')
                    params[param[0]] = param[1]
                }
            }
            return params
        },
        /**
         * @description 获取水平滚动条距离左侧的距离，即文档被拉下的距离. 兼容不同的文档模式
         * @returns {number} 水平滚动条距离顶部的距离
         */
        getScrollLeft: function () {
            return window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft
        },
        /**
         * 获取垂直滚动条距离顶部的距离，即文档被拉下的距离. 兼容不同的文档模式
         * @returns {number}
         */
        getScrollTop: function () {
            return window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop
        },
        /**
         * 清除字符串开头和结尾的空格
         */
        trim: function (str) {
            return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '')
        },
        /**
         * 清除字符串中所有的空格
         * @returns {*}
         */
        trimAll: function (str) {
            return str.replace(/\s+/g, '')
        },
        /**
         * 检测密码强度
         * @param str '12asdASAD'
         * @returns {number} 3(强度等级为3)
         */
        checkPasswordLevel: function (str) {
            var nowLv = 0
            if (str.length < 6) {
                return nowLv
            }
            if (/[0-9]/.test(str)) {
                nowLv++
            }
            if (/[a-z]/.test(str)) {
                nowLv++
            }
            if (/[A-Z]/.test(str)) {
                nowLv++
            }
            if (/[\.|-|_]/.test(str)) {
                nowLv++
            }
            return nowLv
        },
        /**
         * 随机返回一个范围的数字
         * @param n1 返回5-10的随机整数，包括5，10
         * @param n2
         * @returns {number}
         */
        randomNum: function (n1, n2) {
            return Math.round(n1 + Math.random() * (n2 - n1));
        },
        /**
         * 格式化处理字符串
         * @param str abc
         * @param size 1
         * @param delimiter -
         * @returns {*} a-b-c
         */
        formatText: function (str, size, delimiter) {
            var _size = size || 3, _delimiter = delimiter || ','
            var regText = '\\B(?=(\\w{' + _size + '})+(?!\\w))'
            var reg = new RegExp(regText, 'g')
            return str.replace(reg, _delimiter)
        },
        /**
         * 金额大写转换函数
         * @param n 168752632
         * @returns {string} 人民币壹亿陆仟捌佰柒拾伍万贰仟陆佰叁拾贰元整
         */
        toDigit: function (n) {
            var fraction = ['角', '分', '厘']
            var digit = ['零', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖']
            var unit = [
                ['元', '万', '亿'],
                ['', '拾', '佰', '仟']
            ]
            var head = n < 0 ? '欠人民币' : '人民币'
            n = Math.abs(n)
            var s = ''
            for (var i = 0; i < fraction.length; i++) {
                s += (digit[Math.floor(n * 10 * Math.pow(10, i)) % 10] + fraction[i]).replace(/零./, '')
            }
            s = s || '整'
            n = Math.floor(n)
            for (var i = 0; i < unit[0].length && n > 0; i++) {
                var p = ''
                for (var j = 0; j < unit[1].length && n > 0; j++) {
                    p = digit[n % 10] + unit[1][j] + p
                    n = Math.floor(n / 10)
                }
                s = p.replace(/(零.)*零$/, '').replace(/^$/, '零') + unit[0][i] + s
                //s = p + unit[0][i] + s;
            }
            return head + s.replace(/(零.)*零元/, '元').replace(/(零.)+/g, '零').replace(/^整$/, '零元整')
        },
        /**
         * 剩余时间
         * @param endTime
         * @returns {string} 剩余时间6天 2小时 28 分钟20 秒
         */
        getEndTime: function (endTime) {
            var t = +new Date(endTime) - +new Date() //时间差的毫秒数
            var d = 0,
                h = 0,
                m = 0,
                s = 0

            if (t >= 0) {
                d = Math.floor(t / 1000 / 3600 / 24)
                h = Math.floor(t / 1000 / 60 / 60 % 24)
                m = Math.floor(t / 1000 / 60 % 60)
                s = Math.floor(t / 1000 % 60)
            }
            return { d: d, h: h, m: m, s: s }
        },
        /**
         * @description 时间格式化
         * @param date
         * @param fmt
         * @return {*}
         */
        formatDate: function (date, fmt) {
            fmt = fmt || 'yyyy-MM-dd hh:mm:ss'

            var _date = new Date(date),
                _fmt = fmt,
                o = {
                    'M+': _date.getMonth() + 1,
                    'd+': _date.getDate(),
                    'h+': _date.getHours(),
                    'm+': _date.getMinutes(),
                    's+': _date.getSeconds()
                }
            if (/(y+)/.test(_fmt)) {
                _fmt = _fmt.replace(RegExp.$1, (_date.getFullYear() + '').substr(4 - RegExp.$1.length))
            }
            for (let k in o) {
                if (new RegExp('(' + k + ')').test(_fmt)) {
                    _fmt = _fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
                }
            }
            return _fmt
        },
        stringify: function (obj) {
            if (obj === null) {
                return null
            }

            let arr = []

            for (let key in obj) {
                arr.push(key + '=' + obj[key])
            }

            return arr.join('&')
        },
        deepClone: function (source) {
            if (!source && typeof source !== 'object') {
                throw new Error('error arguments deepClone')
            }
            const targetObj = source.constructor === Array ? [] : {}
            Object.keys(source).forEach(keys => {
                if (source[keys] && typeof source[keys] === 'object') {
                    targetObj[keys] = app.deepClone(source[keys])
                } else {
                    targetObj[keys] = source[keys]
                }
            })
            return targetObj
        },
        shareTo: function (options) {
            const { url, title, pics, type, summary, desc, appkey } = options
            // var url = '项目访问地址'
            // var title = '项目名称'
            // var pics = '项目的封面图片地址'
            // var summary = '摘要'
            // var desc = '描述'
            // var appkey = '新浪微博分享appkey'

            // qq空间接口的传参
            if (type === 'qzone') {
                window.open('https://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url=' + url + '?sharesource=qzone&title=' + title + '&pics=' + pics + '&summary=' + summary)
            }
            // 新浪微博接口的传参
            if (type === 'weibo') {
                window.open('http://service.weibo.com/share/share.php?url=' + url + '?sharesource=weibo&title=' + title + '&pic=' + pics + '&appkey=' + appkey)
            }
            // qq好友接口的传参
            if (type === 'qq') {
                window.open('http://connect.qq.com/widget/shareqq/index.html?url=' + url + '?sharesource=qzone&title=' + title + '&pics=' + pics + '&summary=' + summary + '&desc=' + desc)
            }
        },
        vue: function (options) {
            return typeof Vue !== 'undefined' && new Vue(options)
        },
        /**
         * 首字母大写
         * @param word
         * @returns {string}
         */
        capitalize: function (word = '') {
            return word.toString().replace(/^\S/, s => s.toUpperCase())
        },
        /**
         * Base64 编码
         * @param str
         * @returns {string|void}
         */
        btoa: function btoa(str) {
            return str ? window.btoa(unescape(encodeURIComponent(JSON.stringify(str)))) : console.warn('str不能为空')
        },
        /**
         * Base64 解码
         * @param str
         * @returns {string|void}
         */
        atob: function atob(str) {
            return str ? decodeURIComponent(escape(window.atob(str))) : console.warn('str不能为空')
        },
    })

}(window, jQuery));

// ui
;(function (win, $) {
    app.namespace('app.ui')

    $.extend(app.ui, {
        toast: function (msg, callback) {
            typeof laryer !== 'undefined' ? layer.msg(msg, {
                offset: '80px',
                time: 2000
            }, callback) : app.toast(msg, callback)
        }
    })

}(window, jQuery))

// request
;(function (win, $) {
    app.namespace('app.request')
    app.namespace('app.env')

    app.env.BASE_API = '/admin/'

    const service = axios.create({
        baseURL: app.env.BASE_API, // api 的 base_url
        timeout: 5000 // request timeout
    })

    // request interceptor
    service.interceptors.request.use(
        config => {
            const token = app.cookie.get('milu.article.token')

            token && (config.headers['Authorization'] = `Bearer ${token}`) // 让每个请求携带自定义token 请根据实际情况自行修改

            return config
        },
        error => {
            // Do something with request error
            console.log(error) // for debug
            return Promise.reject(error)
        }
    )

    // response interceptor
    service.interceptors.response.use(
        response => {
            const { data } = response

            if (data?.code !== 0) {
                layer.msg(data?.msg || '操作失败', {
                    offset: '80px',
                    time: 2000
                })
                return Promise.reject(data)
            }

            if (!data) {
                message.error(data.msg)
                return Promise.reject(data)
            }

            return Promise.resolve(data)
        },
        error => {
            setTimeout(() => {
                error?.response && handleResponse(error?.response)
            }, 800)
            console.error('Axios response error: ', error)

            layer.msg(error?.response?.data?.msg || error?.msg || '操作失败', {
                offset: '80px',
                time: 2000
            })
            return Promise.reject(error)
        }
    )

    const handleResponse = (response) => {
        switch (response?.status) {
            case 401:
                location.href = '/admin/login'
                break
            default:
                location.href = '/admin'
        }
    }

    app.request = service

}(window, jQuery));

;(function ($) { // jQuery 插件集合
    $.fn.imgSlider = function (options) {
        var defaults = {
                auto: true, num: 3, speed: 4 // speed
            }, that = $(this), size = that.find('li').length, timer = 0, opts = $.extend({}, defaults, options),
            n = opts.num

        if (size > n) {
            var page = size + 1, // pages
                li_w = that.find('li').outerWidth(true), ul_w = li_w * size, ul = that.find('ul')
            ul.append(ul.html()).css({'width': 2 * ul_w, 'marginLeft': -ul_w}) // in order to move forward with pictures

            that.find('.btnRight').bind('click', function () {
                if (!ul.is(':animated')) {
                    if (page < 2 * size - n) { //
                        ul.animate({'marginLeft': '-=' + li_w}, 'slow', 'linear')
                        page++
                    } else {
                        ul.animate({'marginLeft': '-=' + li_w}, 'slow', 'linear', function () {
                            ul.css('marginLeft', (n - size) * li_w)
                            page = (size - n) + 1
                        })
                    }
                }
            })
            that.find('.btnLeft').bind('click', function () {
                if (!ul.is(':animated')) {
                    if (page > 2) {
                        ul.animate({'marginLeft': '+=' + li_w}, 'slow', 'linear')
                        page--
                    } else {
                        ul.animate({'marginLeft': '+=' + li_w}, 'slow', 'linear', function () {
                            ul.css('marginLeft', -ul_w)
                            page = size + 1
                        })
                    }
                }
            })
            that.hover(function () {
                clearInterval(timer)
            }, function () {
                if (opts.auto) {
                    timer = setInterval(function () {
                        that.find('.btnRight').click()
                    }, opts.speed * 1000)
                }
            }).trigger('mouseleave')
        }
    }
    $.fn.utilTab = function (options) {
        var defaults = {
            tag: 'li', // tab 标签名
            subName: '.utilTabSub', // sub class name
            current: 'on',    // 当前的className
            eventType: 'click', // 触发的事件类型
            showType: 'fadeIn' // 触发的效果类型
        }, opts = $.extend({}, defaults, options), that = $(this)

        that.find(opts.tag)[opts.eventType](function () {
            var idx = $(this).index()
            $(this).addClass(opts.current).siblings().removeClass(opts.current)
            that.find(opts.subName).eq(idx)[opts.showType]().siblings(opts.subName).hide()
        })
    }

    $.fn.pop = function (options) {
        var defaults = {
                event: 'click', // event type
                idContent: '',      // content id
                closeBtn: '',      // close id
                isMask: true,    // if the mask
                isMaskClose: true
            }, that = $(this), opts = $.extend({}, defaults, options), content = $(opts.idContent),
            IE6 = $.browser.msie && !window.XMLHttpRequest, closeBtn = $(opts.closeBtn), viewWidth = $(window).width(),
            viewHeight = $(window).height(), left = (viewWidth - content.outerWidth()) / 2,
            top = (viewHeight - content.outerHeight()) / 2, pos = IE6 && 'absolute', sTop = 0, oMask = null

        that[opts.event](function () {
            IE6 && (sTop = $(document).scrollTop())
            content.css({position: pos, left: left + 'px', top: top + sTop + 'px', zIndex: 888}).fadeIn()
            if ($('.ui-mask').length == 0) {
                oMask = $('<div class="ui-mask"></div>').appendTo(document.body)
            } else {
                oMask = $('.ui-mask')
            }
            if (opts.isMask) {
                oMask.css({width: $(window).width(), height: $(document).height()}).fadeTo('normal', 0.5)
                IE6 && oMask.html('<iframe src="about:blank" style="width:100%;height:100%;position:absolute;top:0;left:0;z-index:-1;filter:alpha(opacity=0)"></iframe>') // 添加全屏iframe以防止select穿透
            }
            close()
            oMask.click(function () {
                closeFn()
            })
            return false
        })

        !IE6 && content.find('.pop-title').setDrag() // set the drag

        function close() {
            content.on('click', opts.closeBtn, function () {
                closeFn()
            })
        }

        function closeFn() {
            content.fadeOut()
            if (opts.isMask) {
                oMask.fadeOut('normal')
                IE6 && oMask.find('iframe').remove()
            }
        }
    }
    $.fn.setDrag = function (options) {
        var defaults = {}, that = $(this), opts = $.extend({}, defaults, options), doc = $(document),
            width = $(window).width(), height = $(window).height(), startX = 0, startY = 0, lastX = 0, lastY = 0,
            box = that.parent(), // handler.parentNode
            handler = that[0], drag = {
                down: function (e) {
                    that.css('cursor', 'move')
                    startX = e.clientX - parseInt(box.css('left'))
                    startY = e.clientY - parseInt(box.css('top'))
                    this.setCapture && this.setCapture() // IE to prevent fast drag losing of object
                    doc.on('mousemove', drag.move)
                    doc.on('mouseup', drag.up)
                    return false // chrome to prevent rolling screen, and the loss of the mouse move style
                }, move: function (e) {
                    lastX = e.clientX - startX
                    lastY = e.clientY - startY
                    lastX = Math.max(0, Math.min(width - box.outerWidth(), lastX))
                    lastY = Math.max(0, Math.min(height - box.outerHeight() - 1, lastY))
                    box.css({'top': lastY + 'px', 'left': lastX + 'px'})
                    window.getSelection ? window.getSelection().removeAllRanges() : document.selection.empty() // cancel the selected text
                    e.stopPropagation()
                }, up: function () {
                    // that.css('cursor', 'auto');
                    doc.off('mousemove', drag.move)
                    doc.off('mouseup', drag.up)
                    handler.releaseCapture && handler.releaseCapture() // IE to prevent fast drag losing of object
                }
            }
        that.on('mousedown', drag.down)
    }

    /**
     * 固定定位
     * @param {String} actCls
     */
    $.fn.fixedDiv = function (actCls) {
        var pos = 0, that = $(this), topVal

        if (that.length > 0) {
            topVal = that.offset().top
        }

        function fix() {
            pos = $(document).scrollTop()

            if (pos > topVal) {
                that.addClass(actCls)
                if (!window.XMLHttpRequest) {
                    that.css({
                        position: 'absolute', top: pos
                    })
                }
            } else {
                that.removeClass(actCls)
                if (!window.XMLHttpRequest) {
                    that.css({
                        position: 'static', top: 'auto'
                    })
                }
                var link = $('.yh-topmenu-link')
                if (link.length > 0 && $('.yh-second-menu').is(':visible')) {
                    link.trigger('click')
                }
            }
        }

        fix()

        $(window).scroll(fix)
    }

    $.goTop = function () {
        var backToTopTxt = '', el = $('.backToTop'),
            backToTopEle = !el.length ? $('<div class="backToTop"></div>').appendTo('body') : el,
            backToTopFun = function () {
                var st = $(document).scrollTop()

                st > 0 ? backToTopEle.show() : backToTopEle.hide()
            }

        backToTopEle.text(backToTopTxt).attr('title', backToTopTxt).click(function () {
            $('html,body').animate({scrollTop: 0}, 120)
        })

        $(window).bind('scroll', backToTopFun)
        $(function () {
            backToTopFun()
        })
    }
}(jQuery))

$(function () {
    $.goTop()
})
